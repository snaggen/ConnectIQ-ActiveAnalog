# Active Analog ConnectIQ Watchface
Active Analog aims to be a clean and simple analog watchface, that provides activity information and progress towards your goals using activity circles.

The following status notifications are supported:
* Notifications
* Alarms
* Do not disturb
* Low battery (below 15%, and red below 5%)
* No phone connection

The watchface is released under the GPLv3 license, but if you like this watchface you are welcome to give a donation.

* XMR: 89SjWHdFXJNb61iQn2NVLjjLL2CcPM6iaSKVKswcCBLoLQtAHbhi9si1apKeXruMQqeXnhHvYbPoPSQXPtL9azAeJ81Uq9e
* BTC: 1C23wxgVnSakR59UeXMXCZ6LkKZQ4ZeBo5
* BCH: qrrwjmxmvr84k0jm9dn6gv9zsr3l9fealg4y6x230t
