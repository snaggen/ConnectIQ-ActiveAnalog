//
// Copyright 2016-2017 by Garmin Ltd. or its subsidiaries.
// Subject to Garmin SDK License Agreement and Wearables
// Application Developer Agreement.
//

using Toybox.Graphics;
using Toybox.Lang;
using Toybox.Math;
using Toybox.System;
using Toybox.Time;
using Toybox.Time.Gregorian;
using Toybox.WatchUi;
using Toybox.Application;

var partialUpdatesAllowed = false;

// This implements an analog watch face
// Original design by Austen Harbour
class AnalogView extends WatchUi.WatchFace
{
    var font;
    var isAwake;
    var screenShape;
    var dndIcon;
    var lowBatteryIcon;
    var noBatteryIcon;
    var noPhoneIcon;
    var alarmIcon;
    var alarmCleanIcon;
    var notificationIcon;
    var stepsIcon;
    var floorIcon;
    var activeIcon;
    var offscreenBuffer;
    var dateBuffer;
    var curClip;
    var screenCenterPoint;

    // Initialize variables for this view
    function initialize() {
        WatchFace.initialize();
        screenShape = System.getDeviceSettings().screenShape;
        partialUpdatesAllowed = ( Toybox.WatchUi.WatchFace has :onPartialUpdate );
    }

    // Configure the layout of the watchface for this device
    function onLayout(dc) {
        // Load the custom font we use for drawing the 3, 6, 9, and 12 on the watchface.
        font = WatchUi.loadResource(Rez.Fonts.Fixed4x6);

        // If this device supports the Do Not Disturb feature,
        // load the associated Icon into memory.
        if (System.getDeviceSettings() has :doNotDisturb) {
            dndIcon = WatchUi.loadResource(Rez.Drawables.DoNotDisturbIcon);
        } else {
            dndIcon = null;
        }

        if (System.getDeviceSettings() has :alarmCount) {
            alarmIcon = WatchUi.loadResource(Rez.Drawables.AlarmIcon);
            alarmCleanIcon = WatchUi.loadResource(Rez.Drawables.AlarmCleanIcon);
        } else {
            alarmIcon = null;
            alarmCleanIcon = null;
        }

        if (System.getDeviceSettings() has :notificationCount) {
            notificationIcon = WatchUi.loadResource(Rez.Drawables.NotificationIcon);
        } else {
            notificationIcon = null;
        }

        if (System.getSystemStats() has :battery) {
            lowBatteryIcon = WatchUi.loadResource(Rez.Drawables.LowBatteryIcon);
            noBatteryIcon = WatchUi.loadResource(Rez.Drawables.NoBatteryIcon);
        } else {
            lowBatteryIcon = null;
            noBatteryIcon = null;
        }

        if (System.getDeviceSettings() has :phoneConnected) {
            noPhoneIcon = WatchUi.loadResource(Rez.Drawables.NoPhoneIcon);
        } else {
            noPhoneIcon = null;
        }

        if (ActivityMonitor.getInfo() has :steps) {
            stepsIcon = WatchUi.loadResource(Rez.Drawables.StepIcon);
        } else {
            stepsIcon = null;
        }

        if (ActivityMonitor.getInfo() has :floorsClimbed) {
            floorIcon = WatchUi.loadResource(Rez.Drawables.FloorIcon);
        } else {
            floorIcon = null;
        }

        if (ActivityMonitor.getInfo() has :activeMinutesWeek) {
            activeIcon = WatchUi.loadResource(Rez.Drawables.ActiveIcon);
        } else {
            activeIcon = null;
        }

        //it's for f7 and others 4.x
        if (Toybox.Graphics has :createBufferedBitmap) {
            offscreenBuffer = Graphics.createBufferedBitmap({
                :width=>dc.getWidth(),
                :height=>dc.getHeight()
            }).get();  
            dateBuffer = Graphics.createBufferedBitmap({
                :width=>dc.getWidth(),
                :height=>Graphics.getFontHeight(Graphics.FONT_MEDIUM)
            }).get();
        } else if(Toybox.Graphics has :BufferedBitmap) {
            // Allocate a full screen size buffer with a palette of only 4 colors to draw
            // the background image of the watchface.  This is used to facilitate blanking
            // the second hand during partial updates of the dis
            offscreenBuffer = new Graphics.BufferedBitmap({
                :width=>dc.getWidth(),
                :height=>dc.getHeight()
            });

            // Allocate a buffer tall enough to draw the date into the full width of the
            // screen. This buffer is also used for blanking the second hand. This full
            // color buffer is needed because anti-aliased fonts cannot be drawn into
            // a buffer with a reduced color palette
            dateBuffer = new Graphics.BufferedBitmap({
                :width=>dc.getWidth(),
                :height=>Graphics.getFontHeight(Graphics.FONT_MEDIUM)
            });
        } else {
            offscreenBuffer = null;
        }

        curClip = null;

        screenCenterPoint = [dc.getWidth()/2, dc.getHeight()/2];
    }

    // This function is used to generate the coordinates of the 4 corners of the polygon
    // used to draw a watch hand. The coordinates are generated with specified length,
    // tail length, and width and rotated around the center point at the provided angle.
    // 0 degrees is at the 12 o'clock position, and increases in the clockwise direction.
    function generateHandCoordinates(centerPoint, angle, handLength, tailLength, width) {
        // Map out the coordinates of the watch hand
        var coords = [[-(width / 2), tailLength], [-(width / 2), -handLength], [width / 2, -handLength], [width / 2, tailLength]];
        var result = new [4];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        // Transform the coordinates
        for (var i = 0; i < 4; i += 1) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + 0.5;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + 0.5;

            result[i] = [centerPoint[0] + x, centerPoint[1] + y];
        }

        return result;
    }

    function drawHand(dc, centerPoint, angle, handLength, tailLength, width, color) {
        var r = Math.floor(width/2);
        var tailLineLength = tailLength - r;
        var handLineLength = handLength - r;

        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        var sX = centerPoint[0] - tailLineLength * sin;
        var sY = centerPoint[1] + tailLineLength * cos;

        var eX = centerPoint[0] + handLineLength * sin;
        var eY = centerPoint[1] - handLineLength * cos;
        var drawOutline = true;

        if (drawOutline) {
            dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
            dc.fillCircle(sX, sY, r+1);
            dc.fillCircle(eX, eY, r+1);

            dc.setPenWidth(width+2);
            dc.drawLine(sX, sY, eX, eY);
        }
        dc.setColor(color, Graphics.COLOR_TRANSPARENT);
        dc.fillCircle(sX, sY, r);
        dc.fillCircle(eX, eY, r);

        dc.setPenWidth(width);
        dc.drawLine(sX, sY, eX, eY);
    }

    function drawHands(dc) {
        var width = dc.getWidth();
        var height = dc.getHeight();
        var clockTime = System.getClockTime();
        var minuteHandAngle;
        var hourHandAngle;
        var thickness = (width/40);
        var tailLength = 0;
        var minuteHandLength = (height/2) - 20; //20 is the hashmarks + some space
        var hourHandLength = (height/4);
        // Draw the hour hand. Convert it to minutes and compute the angle.
        hourHandAngle = (((clockTime.hour % 12) * 60) + clockTime.min);
        hourHandAngle = hourHandAngle / (12 * 60.0);
        hourHandAngle = hourHandAngle * Math.PI * 2;
        drawHand(dc, screenCenterPoint, hourHandAngle, hourHandLength, tailLength, thickness, Graphics.COLOR_WHITE);

        //dc.fillPolygon(generateHandCoordinates(screenCenterPoint, hourHandAngle, 65, 10, 5));

        // Draw the minute hand.
        minuteHandAngle = (clockTime.min / 60.0) * Math.PI * 2;
        drawHand(dc, screenCenterPoint, minuteHandAngle, minuteHandLength, tailLength, thickness, Graphics.COLOR_WHITE);
        //dc.fillPolygon(generateHandCoordinates(screenCenterPoint, minuteHandAngle, 90, 10, 5));

        // Draw the arbor in the center of the screen.
        //dc.setPenWidth(1);
        //dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
        //dc.fillCircle(screenCenterPoint[0], screenCenterPoint[1], Math.floor(thickness/2));
        //dc.setColor(Graphics.COLOR_DK_GRAY,Graphics.COLOR_BLACK);
        //dc.drawCircle(screenCenterPoint[0], screenCenterPoint[1], Math.floor(thickness/2)+1);
    }

    function drawDate(dc) {
        var width = dc.getWidth();
        var height = dc.getHeight();
        var info = Gregorian.info(Time.now(), Time.FORMAT_MEDIUM);
        var dateStr = Lang.format("$1$ $2$", [info.day_of_week, info.day]);

        //dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_BLACK);
        //dc.fillRoundedRectangle(width - 25 , (height/2 - 10), 25, 24, 5);
        //dc.setColor(Graphics.COLOR_DK_GRAY,Graphics.COLOR_TRANSPARENT);
        //dc.drawRoundedRectangle(width - 25 , (height/2 - 10), 25, 24, 5);
        dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT);
        //dc.drawText(width-12, height/2 - 12, Graphics.FONT_XTINY, "30", Graphics.TEXT_JUSTIFY_CENTER);
        dc.drawText(width/2, (height/2) - (height/4), Graphics.FONT_XTINY, dateStr, Graphics.TEXT_JUSTIFY_CENTER);
    }

    function drawStatusBar(dc) {
        var padding = 3;
        var width = dc.getWidth();
        var height = dc.getHeight();
        // Draw the do-not-disturb icon if we support it and the setting is enabled
        var numberOfIcons = 0;
        if (null != dndIcon && System.getDeviceSettings().doNotDisturb) {
            numberOfIcons++;
        }
        if (null != notificationIcon && System.getDeviceSettings().notificationCount) {
            numberOfIcons++;
        }
        if (null != alarmIcon && System.getDeviceSettings().alarmCount) {
            numberOfIcons++;
        }
        if (null != lowBatteryIcon && System.getSystemStats().battery < 15.0) {
            numberOfIcons++;
        }
        if (null != noPhoneIcon && !System.getDeviceSettings().phoneConnected) {
            numberOfIcons++;
        }
        if (numberOfIcons == 0) {
            return;
        }

        var drawStart = width/2 - (numberOfIcons*16/2);
        var vPos = (height / 4) - (height/9);
        if (null != dndIcon && System.getDeviceSettings().doNotDisturb) {
            dc.drawBitmap( drawStart, vPos, dndIcon);
            drawStart += 16 + padding;
        }

        var notificationCount = System.getDeviceSettings().notificationCount;
        if (null != notificationIcon && notificationCount) {
            dc.drawBitmap( drawStart, vPos, notificationIcon);
            if (notificationCount > 1 && notificationCount <= 99) {
                dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
                dc.drawText(drawStart+8, vPos+3, font, notificationCount, Graphics.TEXT_JUSTIFY_CENTER);
            }
            drawStart += 16 + padding;
        }

        var alarmCount = System.getDeviceSettings().alarmCount;
        if (null != alarmIcon && alarmCount) {
            if (alarmCount > 1 && alarmCount <= 99) {
                dc.drawBitmap( drawStart, vPos, alarmCleanIcon);
                dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
                dc.drawText(drawStart+9, vPos+5, font, alarmCount, Graphics.TEXT_JUSTIFY_CENTER);
            } else {
                dc.drawBitmap( drawStart, vPos, alarmIcon);
            }
            drawStart += 16 + padding;
        }

        if (null != noBatteryIcon && System.getSystemStats().battery <= 5.0) {
            dc.drawBitmap( drawStart, vPos, noBatteryIcon);
            drawStart += 16 + padding;
        } else if (null != lowBatteryIcon && System.getSystemStats().battery <= 15.0) {
            dc.drawBitmap( drawStart, vPos, lowBatteryIcon);
            drawStart += 16 + padding;
        }
        if (null != noPhoneIcon && !System.getDeviceSettings().phoneConnected) {
            dc.drawBitmap( drawStart, vPos, noPhoneIcon);
            drawStart += 16 + padding;
        }
    }

    function drawActivityCircles(dc) {
        var width = dc.getWidth();
        var height = dc.getHeight();
        var handTailSize = (width/30); //FIXME: Cludge to get start and stop of progress pretty
        var largeTickSize = 10;
        var padding = 3;
        var stepOuterRadius = Math.floor(((height/2) - handTailSize - largeTickSize - (2*padding))/2);
        var stepInnerRadius = stepOuterRadius - 5;
        var sideOuterRadius = stepOuterRadius - 10;
        var sideInnerRadius = sideOuterRadius - 5;
        var vCenter = height/2 + handTailSize + stepOuterRadius;
        var r = vCenter - height/2;


        /* Active minutes circle */
        if (activeIcon != null) {
            var leftAngle = (260.0 / 360) * Math.PI * 2;
            var lX = (width/2 + r * Math.sin(leftAngle)).toLong();
            var lY = (height/2 - r * Math.cos(leftAngle)).toLong();
            dc.setPenWidth(1);
            dc.setColor(Graphics.COLOR_DK_GRAY,Graphics.COLOR_BLACK);
            dc.drawArc(lX, lY,sideOuterRadius,Graphics.ARC_COUNTER_CLOCKWISE, 354, 286);

            var activeMinsGoal = ActivityMonitor.getInfo().activeMinutesWeekGoal;
            var activeMins = ActivityMonitor.getInfo().activeMinutesWeek;
            var activePercent = 1.0;
            if (null != activeMins && null != activeMinsGoal && activeMins.total <= activeMinsGoal) {
                activePercent = activeMins.total.toDouble()/activeMinsGoal;
            }
            dc.setColor(Graphics.COLOR_ORANGE,Graphics.COLOR_BLACK);
            dc.setPenWidth(4);
            var aStart = 351;
            var len = 296;
            if (activePercent >= 1.0) {
                len = 300;
            }
            var aEnd = (aStart + activePercent * len).toLong()%360;
            if (aStart != aEnd) {
                dc.drawArc(lX,lY,sideInnerRadius,Graphics.ARC_COUNTER_CLOCKWISE, aStart, aEnd);
            }
            dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT);
            dc.drawText(lX, lY-4, Graphics.FONT_XTINY, activeMins.total, Graphics.TEXT_JUSTIFY_CENTER);
            if (null != activeIcon) {
                dc.drawBitmap(lX-8, lY-24, activeIcon);
            }
        }

        /* Floor Circle */
        if (floorIcon != null) {
            var rightAngle = (100.0 / 360) * Math.PI * 2;
            var rX = (width/2 + r * Math.sin(rightAngle)).toLong();
            var rY = (height/2 - r * Math.cos(rightAngle)).toLong();
            dc.setPenWidth(1);
            dc.setColor(Graphics.COLOR_DK_GRAY,Graphics.COLOR_BLACK);
            dc.drawArc(rX, rY,sideOuterRadius,Graphics.ARC_CLOCKWISE, 183, 258);

            var floorsClimbedGoal = ActivityMonitor.getInfo().floorsClimbedGoal;
            var floorsClimbed = ActivityMonitor.getInfo().floorsClimbed;
            var floorsPercent = 1.0;
            if (null != floorsClimbed && null != floorsClimbedGoal && floorsClimbed <= floorsClimbedGoal) {
                floorsPercent = floorsClimbed.toDouble()/floorsClimbedGoal;
            }
            dc.setColor(Graphics.COLOR_GREEN,Graphics.COLOR_BLACK);
            dc.setPenWidth(4);
            var fStart = 188;
            var len = 294;
            if (floorsPercent >= 1.0) {
                len = 300;
            }
            var fEnd = (fStart - floorsPercent * len).toLong()%360;
            if (fStart != fEnd) {
                dc.drawArc(rX,rY,sideInnerRadius,Graphics.ARC_CLOCKWISE, fStart, fEnd);
            }
            dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT);
            if (null != floorIcon) {
                dc.drawBitmap(rX-8, rY-24, floorIcon);
            }
            dc.drawText(rX, rY-4, Graphics.FONT_XTINY, floorsClimbed, Graphics.TEXT_JUSTIFY_CENTER);
        }

        /* Steps Cirle */
        if (stepsIcon) {
            dc.setPenWidth(1);
            dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_BLACK);
            dc.fillCircle(width/2, vCenter, stepOuterRadius);
            dc.setColor(Graphics.COLOR_DK_GRAY,Graphics.COLOR_BLACK);
            dc.drawCircle(width/2, vCenter, stepOuterRadius);

            var stepGoal = ActivityMonitor.getInfo().stepGoal;
            var steps = ActivityMonitor.getInfo().steps ;
            var stepsPercent = 1.0;
            if (null != steps && null != stepGoal && steps <= stepGoal) {
                stepsPercent = steps.toDouble()/stepGoal;
            }
            dc.setColor(Graphics.COLOR_PURPLE,Graphics.COLOR_BLACK);
            dc.setPenWidth(4);
            var sStart = 90;
            var sEnd = (360 - (stepsPercent*360).toLong()+90)%360;
            if (sStart != sEnd || stepsPercent >= 1.0) {
                dc.drawArc(width/2,vCenter,stepInnerRadius,Graphics.ARC_CLOCKWISE, sStart, sEnd);
            }
            dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT);
            if (null != stepsIcon) {
                dc.drawBitmap((width/2)-12, vCenter-30, stepsIcon);
            }
            dc.drawText((width / 2), vCenter-4, Graphics.FONT_XTINY, steps, Graphics.TEXT_JUSTIFY_CENTER);
        }
    }

    // Draws the clock tick marks around the outside edges of the screen.
    function drawHashMarks(dc) {
        var width = dc.getWidth();
        var height = dc.getHeight();

        // Draw hashmarks differently depending on screen geometry.
        if (System.SCREEN_SHAPE_ROUND == screenShape) {
            var sX, sY;
            var eX, eY;
            var outerRad = width / 2;
            var innerRad = outerRad - 10;
            var smallInnerRad = outerRad - 5;
            for (var j = 0; j < 60; j++) {
                var iRad = smallInnerRad;
                if (j%5==0) {
                    iRad = innerRad;
                    dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_LT_GRAY);
                    dc.setPenWidth(3);
                }
                var i = j * Math.PI / 30;
                sY = outerRad + iRad * Math.sin(i);
                eY = outerRad + outerRad * Math.sin(i);
                sX = outerRad + iRad * Math.cos(i);
                eX = outerRad + outerRad * Math.cos(i);
                dc.drawLine(sX, sY, eX, eY);
                if (j%5==0) {
                    dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_DK_GRAY);
                    dc.setPenWidth(1);
                }
            }
        } else {
            //Not supported
            System.println( "Only round watchfaces are supported!");
        }
    }

    // Handle the update event
    function onUpdate(dc) {
        var width;
        var height;
        var screenWidth = dc.getWidth();
        var secondHand;
        var targetDc = null;

        if(null != offscreenBuffer) {
            dc.clearClip();
            curClip = null;
            // If we have an offscreen buffer that we are using to draw the background,
            // set the draw context of that buffer as our target.
            targetDc = offscreenBuffer.getDc();
        } else {
            targetDc = dc;
        }

        width = targetDc.getWidth();
        height = targetDc.getHeight();

        // Fill the entire background with Black.
        targetDc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_WHITE);
        targetDc.fillRectangle(0, 0, dc.getWidth(), dc.getHeight());

        // Draw the tick marks around the edges of the screen
        drawHashMarks(targetDc);

        drawStatusBar(targetDc);

        drawDate(targetDc);

        drawActivityCircles(targetDc);

        drawHands(targetDc);

        // Output the offscreen buffers to the main display if required.
        drawBackground(dc);
    }

    // Handle the partial update event
    function onPartialUpdate( dc ) {
    }

    // Draw the watch face background
    // onUpdate uses this method to transfer newly rendered Buffered Bitmaps
    // to the main display.
    // onPartialUpdate uses this to blank the second hand from the previous
    // second before outputing the new one.
    function drawBackground(dc) {
        var width = dc.getWidth();
        var height = dc.getHeight();

        //If we have an offscreen buffer that has been written to
        //draw it to the screen.
        if( null != offscreenBuffer ) {
            dc.drawBitmap(0, 0, offscreenBuffer);
        }
    }

    // This method is called when the device re-enters sleep mode.
    // Set the isAwake flag to let onUpdate know it should stop rendering the second hand.
    function onEnterSleep() {
        isAwake = false;
        WatchUi.requestUpdate();
    }

    // This method is called when the device exits sleep mode.
    // Set the isAwake flag to let onUpdate know it should render the second hand.
    function onExitSleep() {
        isAwake = true;
    }
}

class AnalogDelegate extends WatchUi.WatchFaceDelegate {

    function initialize() {
        WatchFaceDelegate.initialize();
    }
    // The onPowerBudgetExceeded callback is called by the system if the
    // onPartialUpdate method exceeds the allowed power budget. If this occurs,
    // the system will stop invoking onPartialUpdate each second, so we set the
    // partialUpdatesAllowed flag here to let the rendering methods know they
    // should not be rendering a second hand.
    function onPowerBudgetExceeded(powerInfo) {
        System.println( "Average execution time: " + powerInfo.executionTimeAverage );
        System.println( "Allowed execution time: " + powerInfo.executionTimeLimit );
        partialUpdatesAllowed = false;
    }
}
